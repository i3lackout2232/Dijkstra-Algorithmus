package de.fi17d.dijkstraalgorithmus.main;

import de.fi17d.dijkstraalgorithmus.gui.Gui;

public class DijkstraAlgorithmus {
    public static void main(String[] args) {
        Gui gui = new Gui();
        gui.createGui();
    }
}
