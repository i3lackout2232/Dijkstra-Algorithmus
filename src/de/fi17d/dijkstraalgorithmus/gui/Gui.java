package de.fi17d.dijkstraalgorithmus.gui;

import de.fi17d.dijkstraalgorithmus.*;
import de.fi17d.dijkstraalgorithmus.Knoten.Graph;
import de.fi17d.dijkstraalgorithmus.Knoten.Kante;
import de.fi17d.dijkstraalgorithmus.Knoten.Knoten;
import de.fi17d.dijkstraalgorithmus.calc.calc;
import de.fi17d.dijkstraalgorithmus.shortesway.shortestway;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.*;
import java.util.List;

public class Gui extends JFrame {

    /**
     * x & y müssen Strings sein, da die Inputfelder nur String liefern.
     * werden danach zum int gewandelt
     */
    public String method, stringX, stringY;
    public int x, y;
    private int[][] matrixcalc;
    private List<JTextField> inputValues = new ArrayList<JTextField>();
    private JTextField start = new JTextField();
    private JTextField end = new JTextField();
    public List<String> alphabet = Arrays.asList("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z");
    private static JFrame mainFrame = new JFrame();
    private static JComboBox selectMethod;
    private JPanel panelTop = new JPanel();
    private JPanel panelBottom = new JPanel();
    private JPanel panelTopGroup = new JPanel();
    private JPanel panelCenter = new JPanel();
    private JLabel lblX = new JLabel();
    private JTextField txtX = new JTextField(5);
    private JButton createBtn = new JButton();
    private JButton calculateBtn = new JButton();
    private String[] methods = { "Tabelle Berechnen", "Kürzester Weg" };
    private static JTextField matrixInput;
    public boolean lockUpdate = false;

    public void createGui() {
        mainFrame.setSize(720, 480);
        selectMethod = new JComboBox(methods); // Dropdown in GUI
        selectMethod.setSelectedIndex(0); // Selektiert die erste Auswahlmöglichkeit automatisch

        /**
         * Panel Top (Größe X & Y, Button Feld generieren)
         */
        panelTop.add(panelTopGroup);
        panelTop.add(panelTopGroup);
        panelTop.add(createBtn);

        lblX.setText("Größe: ");
        createBtn.setText("Generieren");
        createBtn.addActionListener(getXandY);
        panelTopGroup.add(lblX);
        panelTopGroup.add(txtX);

        /**
         * Panel Bottom (Select, Button Calculate)
         */
        calculateBtn.setText("Berechnen");
        calculateBtn.addActionListener(calculateMatrix);
        panelBottom.add(selectMethod);
        panelBottom.add(calculateBtn);

        /**
         * Komplettes Frame
         */
        mainFrame.add(panelTop, BorderLayout.PAGE_START);
        mainFrame.add(panelCenter, BorderLayout.CENTER);
        mainFrame.add(panelBottom, BorderLayout.PAGE_END);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setLocationRelativeTo(null); // Fenster zentrieren
        mainFrame.setResizable(false); // feste Größe, nicht veränderbar
        mainFrame.setVisible(true);
    }

    private void generateCenterPanel() {
        panelCenter.removeAll(); // leeren des Feldes
        panelCenter.setLayout(new GridLayout(x,y));
        inputValues = new ArrayList();
        for (int i = 0; i < x*y; i++) { //generieren der Inputfelder
            matrixInput = new JTextField();
            matrixInput.setName(String.valueOf(i)); // setzt einen Namen für die generierten Inputfelder
            matrixInput.getDocument().addDocumentListener(setReflection);
            panelCenter.add(matrixInput); // im Panel registrieren
            inputValues.add(matrixInput); // setzt die Inputfelder in ein Array
        }

        for(int i = 0; i < x; i++) {
            for(int j = 0; j < x; j++) {
                if (i < j) {
                    //System.out.println(i*x+j);
                    inputValues.get(j*x+i).setEditable(false);
                    inputValues.get(j*x+i).setFocusable(false);
                }
                if(i == j) {
                    inputValues.get(j*x+i).setEditable(false);
                    inputValues.get(j*x+i).setText("0");
                    inputValues.get(j*x+i).setFocusable(false);
                }
            }
        }

        /**
         * Anzeige aktualisieren
         */
        panelCenter.revalidate();
        panelCenter.repaint();
        return;
    }

    private DocumentListener setReflection = new DocumentListener() {
        public void insertUpdate(DocumentEvent e) {
            update();
        }

        public void removeUpdate(DocumentEvent e) {
            update();
        }

        public void changedUpdate(DocumentEvent e) {
            update();
        }

        private void update() {
            if (lockUpdate) {
                return;
            } else {
                lockUpdate = true;
            }
            for(int i = 0; i < x; i++) {
                for(int j = 0; j < x; j++) {
                    if (i > j) {
                        System.out.println(i*x+j);
                        inputValues.get(i*x+j).setText(inputValues.get(j*x+i).getText());
                    }
                }
            }
            lockUpdate = false;
        }
    };

    private ActionListener getXandY = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            /**
             * Prüfen, ob der richtige Button gedrückt wurde
             *
             * Wenn werte gesetzt sind, holt er die Daten aus dem Inputfeld und wandelt die Zahl von einen String
             * in einen Integer um
             *
             * danach der Aufruf der Methode zum generieren der Felder
             */
            if(e.getSource() == createBtn) {
                try {
                    if (Integer.parseInt(txtX.getText()) <= 26 && Integer.parseInt(txtX.getText()) >= 2) {
                        stringX = txtX.getText();
                        x = Integer.parseInt(stringX);
                        stringY = txtX.getText();
                        y = Integer.parseInt(stringY);
                        Object[] message = {
                                "Startknoten:", start,
                                "Endknoten:", end
                        };
                        int option = JOptionPane.showConfirmDialog(new JFrame(), message,"Start- und Endknoten", JOptionPane.OK_CANCEL_OPTION);
                        if (option == JOptionPane.OK_OPTION) {
                            if (!start.getText().equals("") && !end.getText().equals("")) {
                                if (alphabet.indexOf(start.getText().toLowerCase()) < Integer.parseInt(txtX.getText()) &&
                                        alphabet.indexOf(end.getText().toLowerCase()) < Integer.parseInt(txtX.getText())) {
                                    generateCenterPanel();
                                } else {
                                    JOptionPane.showMessageDialog(new JFrame(), "Start- und/oder Endknoten ist nicht vorhanden!","Fehler", JOptionPane.ERROR_MESSAGE);
                                }
                            } else {
                                JOptionPane.showMessageDialog(new JFrame(), "Start- und/oder Endknoten ist nicht vorhanden!","Fehler", JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    } else {
                        throw new NumberFormatException();
                    }
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(new JFrame(), "Bitte geben sie eine Zahl zwischen 2 und 26 ein!","Fehler", JOptionPane.ERROR_MESSAGE);
                }

            }
        }
    };

    @SuppressWarnings("Duplicates") //nur für die die IDE relevant, damit das unterstreichen von "gleichen" Code aus ist
            ActionListener calculateMatrix = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            /**
             * Prüfen, ob der richtige Button gedrückt wurde, Das Feld generiert wurde
             * und ob alle Inputfelder gefühlt sind
             */
            if(e.getSource() == calculateBtn) {
                // Prüfen ob Feld generiert wurde
                if(inputValues.size() == 0) {
                    return;
                }
                // Prüfen ob alle Inputfelder gefühlt sind
                for (int i = 0; i < inputValues.size(); i++) {
                    if(inputValues.get(i).getText() == "" || inputValues.get(i).getText().equals("")) {
                        return;
                    }
                }

                // holen der Werte aus dem Array
                matrixcalc = new int[x][y];
                for(int i = 0; i < x; i++) {
                    for(int j = 0; j < y; j++) {
                        matrixcalc[i][j] = Integer.parseInt(inputValues.get(i*x+j).getText());
                    }
                }

                /**
                 * Überprüfen, was im Dropdown ausgewählt wurde
                 */
                if(selectMethod.getSelectedIndex() == 0) {
                    panelCenter.removeAll();
                    calc c = new calc(getGraphFromMatrix(), start.getText().toLowerCase(), end.getText().toLowerCase());
                    List<Knoten> knotens = c.Calculate();

                    for(int i = 0; i < knotens.size(); i++) {
                        panelCenter.add(new JLabel(knotens.get(i).getName() + " | " +
                                knotens.get(i).getValue()  + " | " +
                                knotens.get(i).getVorgaenger()));
                    }

                    panelCenter.revalidate();
                    panelCenter.repaint();

                } else if(selectMethod.getSelectedIndex() == 1) {
                    panelCenter.removeAll();
                    calc c = new calc(getGraphFromMatrix(), start.getText().toLowerCase(), end.getText().toLowerCase());
                    List<Knoten> knotens = c.Calculate();
                    shortestway s = new shortestway(knotens, start.getText().toLowerCase(), end.getText().toLowerCase());
                    panelCenter.add(new JLabel(s.getShortestWay()));

                    panelCenter.revalidate();
                    panelCenter.repaint();
                }
            }
        }
    };

    private Graph getGraphFromMatrix() {
        Graph g = new Graph();
        for (int i = 0; i < matrixcalc.length; i++) {
            if (alphabet.get(i).equals(start.getText().toLowerCase())) {
                g.addKnoten(new Knoten(alphabet.get(i), 0, false, ""));
            } else {
                g.addKnoten(new Knoten(alphabet.get(i), Integer.MAX_VALUE, false, ""));
            }

        }

        for(int i = 0; i < matrixcalc.length; i++) {
            for(int j = 0; j < matrixcalc.length; j++) {
                if (j > i && matrixcalc[i][j] != 0) {
                    g.addKante(new Kante(alphabet.get(i), alphabet.get(j), matrixcalc[i][j]));
                }
            }
        }
        return g;
    }
}
