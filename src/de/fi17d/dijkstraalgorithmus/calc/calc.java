package de.fi17d.dijkstraalgorithmus.calc;

import de.fi17d.dijkstraalgorithmus.Knoten.Graph;
import de.fi17d.dijkstraalgorithmus.Knoten.Kante;
import de.fi17d.dijkstraalgorithmus.Knoten.Knoten;

import java.util.List;

public class calc {

	private Graph g=null;
	private String Startknoten="";
	private String Endknoten="";
	
	public calc(Graph g, String Startknoten, String Endknoten) {
		this.g=g;
		this.Startknoten=Startknoten;
		this.Endknoten=Endknoten;
	}

	// Methode für das Ausführen des Algorithmus
	public List<Knoten> Calculate() {
		//Solange der Endknoten noch nicht besucht wurde
		while(!g.Done(Endknoten)) {
			//aktueller Knoten wird auf den unbesuchten Knoten mit dem kleinsten Wert gesetzt
			Knoten aktuellerKnoten=g.GetNextKnoten();
			//die Variable 'besucht' des aktuellen Knoten wird auf true gesetzt
			aktuellerKnoten.setBesucht(true);
			//falls der aktuelle Knoten der Endknoten ist, wird abgebrochen
			if (aktuellerKnoten.getName().equals(Endknoten)) {
                //Rückgabe der bearbeiteten Knotenliste
				return g.getKnotenListe();
			}
			//Liste mit allen Nachbarkanten
			List<Kante> Nachbarkanten=g.GetNachbarkanten(aktuellerKnoten.getName());
			//Iteration über alle Nachbarkanten
			for(int i=0;i<Nachbarkanten.size();i++) {
				//Zwischenspeichern der aktuellen Kante
				Kante k=Nachbarkanten.get(i);
				//Falls der aktuelle Knoten der erste Knoten der Kante ist
				if(k.getKnoten1().equals(aktuellerKnoten.getName())) {
					//Falls der Wert des aktuellen Knotens + der Wert der Kante kleiner ist als der Wert des zweiten Knotens
					if(aktuellerKnoten.getValue()+k.getValue()<g.GetKnotenByName(k.getKnoten2()).getValue()) {
						//Ersetzen des Werts des zweiten Knotens durch den Wert des aktuellen Knotens+ den Wert der Kante
						g.GetKnotenByName(k.getKnoten2()).setValue(aktuellerKnoten.getValue()+k.getValue());
						//Setzen des Vorgängers des zweiten Knotens auf den aktuellen Knoten
						g.GetKnotenByName(k.getKnoten2()).setVorgaenger(aktuellerKnoten.getName());
					}
				}
				//ansonsten falls der aktuelle Knoten der zweite Knoten der Kante ist
				else if(k.getKnoten2().equals(aktuellerKnoten.getName())) {
					//Falls der Wert des aktuellen Knotens + der Wert der Kante kleiner ist als der Wert des ersten Knotens
					if(aktuellerKnoten.getValue()+k.getValue()<g.GetKnotenByName(k.getKnoten1()).getValue()) {
						//Ersetzen des Werts des ersten Knotens durch den Wert des aktuellen Knotens+ den Wert der Kante
						g.GetKnotenByName(k.getKnoten1()).setValue(aktuellerKnoten.getValue()+k.getValue());
						//Setzen des Vorgängers des ersten Knotens auf den aktuellen Knoten
						g.GetKnotenByName(k.getKnoten1()).setVorgaenger(aktuellerKnoten.getName());
					}
				}
			}
		}
		//Rückgabe der bearbeiteten Knotenliste
		return g.getKnotenListe();
	}
}