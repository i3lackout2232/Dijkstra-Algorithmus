package de.fi17d.dijkstraalgorithmus.Knoten;

public class Kante {

    private String Knoten1;
    private String Knoten2;
    private Integer value;

    public Kante(final String Knoten1, final String Knoten2, final Integer value) {
        this.Knoten1 = Knoten1;
        this.Knoten2 = Knoten2;
        this.value = value;
    }

    public String getKnoten1() {
        return Knoten1;
    }

    public String getKnoten2() {
        return Knoten2;
    }

    public Integer getValue() {
        return value;
    }

    public void setKnoten1(String knoten1) {
        Knoten1 = knoten1;
    }

    public void setKnoten2(String knoten2) {
        Knoten2 = knoten2;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
