package de.fi17d.dijkstraalgorithmus.Knoten;

public class Knoten {

    private String name;
    private Integer value;
    private Boolean besucht;
    private String vorgaenger;

    public Knoten(final String name, final Integer value, final Boolean besucht, final String vorgaenger) {
        this.name = name;
        this.value = value;
        this.besucht = besucht;
        this.vorgaenger = vorgaenger;
    }

    public String getName() {
        return name;
    }

    public Integer getValue() {
        return value;
    }

    public Boolean getBesucht() {
        return besucht;
    }

    public String getVorgaenger() {
        return vorgaenger;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public void setBesucht(Boolean besucht) {
        this.besucht = besucht;
    }

    public void setVorgaenger(String vorgaenger) {
        this.vorgaenger = vorgaenger;
    }
}
