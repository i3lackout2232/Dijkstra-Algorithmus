package de.fi17d.dijkstraalgorithmus.Knoten;

import java.util.ArrayList;
import java.util.List;

public class Graph {

    private List<Knoten> KnotenListe = new ArrayList<Knoten>();
    private List<Kante> KantenListe = new ArrayList<Kante>();

    public Graph() {
        this.KnotenListe = new ArrayList<Knoten>();
        this.KantenListe = new ArrayList<Kante>();
    }

    public List<Kante> getKantenListe() {
        return KantenListe;
    }

    public List<Knoten> getKnotenListe() {
        return KnotenListe;
    }

    public void addKante(final Kante kante) {
        if (!this.KantenListe.contains(kante)) {
            this.KantenListe.add(kante);
        }
    }

    public void addKnoten(final Knoten knoten)  {
        if(!this.KnotenListe.contains(knoten)) {
            this.KnotenListe.add(knoten);
        }
    }

    // Methode zur Überprüfung, ob eine Abbruchbedingung erfüllt ist
    public Boolean Done(String Endknoten) {
        //Falls der Endknoten besucht wurde, wird true zurückgegeben
        if(GetKnotenByName(Endknoten).getBesucht()) {
            return true;
        }
        //Folgende Überprüfung ist überflüssig, allerdings haben wir uns an das Arbeitsblatt gehalten...
        //Iteration über alle Knoten
        for(int i=0;i<KnotenListe.size();i++) {
            //Falls ein Knoten noch nicht besucht wurde, wird false zurückgegeben
            if(!KnotenListe.get(i).getBesucht()) {
                return false;
            }
        }
        //Falls alle Knoten besucht wurden, wird true zurückgegeben
        return true;
    }

    // Methode zum Abrufen von Knoten anhand ihres Namens
    public Knoten GetKnotenByName(String s) {
        //Iteration über alle Knoten
        for(int i=0;i<KnotenListe.size();i++) {
            //Falls der Name des Knotens dem gesuchten Namen entspricht, wird dieser zurückgegeben
            if(KnotenListe.get(i).getName().equals(s)) {
                return KnotenListe.get(i);
            }
        }
        //Falls kein Knoten mit dem gesuchtem Namen existiert wird null zurückgegeben
        return null;
    }

    // Methode zum Abrufen des als nächstes zu besuchenden Knotens
    public Knoten GetNextKnoten() {
        //Variable zum Zwischenspeichern des Knotens
        Knoten temp=null;
        //Iteration über alle Knoten
        for(int i=0;i<KnotenListe.size();i++) {
            /* Falls der Knoten nicht besucht wurde und noch kein Knoten zwischengespeichert wurde,
               oder der Wert des zwischengespeicherten Knotens größer ist als der Wert des Knotens */
            if(!KnotenListe.get(i).getBesucht() && (temp==null||temp.getValue()>KnotenListe.get(i).getValue())) {
                //Zwischenspeichern des Knotens
                temp=KnotenListe.get(i);
            }
        }
        //Rückgabe des zwischengespeicherten Knotens
        return temp;
    }

    // Methode zum Abrufen der Nachbarkanten
    public List<Kante> GetNachbarkanten(String aktuellerKnoten) {
        //Liste für die Nachbarkanten
        List<Kante> Nachbarkanten=new ArrayList<Kante>();
        //Iteration über alle Kanten
        for(int i=0;i<KantenListe.size();i++) {
            //Falls der aktuelle Knoten der erste oder zweite Knoten ist, wird die Kante der Liste hinzugefügt
            if(KantenListe.get(i).getKnoten1().equals(aktuellerKnoten) ||
                    KantenListe.get(i).getKnoten2().equals(aktuellerKnoten)) {
                Nachbarkanten.add(KantenListe.get(i));
            }
        }
        //Rückgabe der Liste
        return Nachbarkanten;
    }
}
