package de.fi17d.dijkstraalgorithmus.shortesway;

import de.fi17d.dijkstraalgorithmus.Knoten.Knoten;

import java.util.ArrayList;
import java.util.List;

public class shortestway {
	public List<Knoten> knotenListe;
	public String knotenstart;
	public String knotenend;

	public shortestway(List<Knoten> knotenListe, String knotenstart, String knotenend) {
		this.knotenListe = knotenListe;
		this.knotenstart = knotenstart;
		this.knotenend = knotenend;
	}

	//Methode zum Ermitteln des kürzesten Wegs
	public String getShortestWay() {
		//String für den kürzesten Weg
		String shortestWay = knotenend;
		//Solange noch nicht der Startknoten erreicht wurde
		while(!knotenend.equals(knotenstart)) {
			//Iteration über alle Knoten
			for (int i = 0; i < knotenListe.size(); i++) {
				//Falls der Knoten den Namen des Endknotens hat
				if(knotenListe.get(i).getName().equals(knotenend)) {
					//Endknoten wird auf den Vorgänger des aktuellen Endknotens gesetzt
					knotenend = knotenListe.get(i).getVorgaenger();
					//Hinzufügen des Wegstücks zum String
					shortestWay = knotenend + "->" + shortestWay;
					//Abbruch der Iteration
					break;
				}
			}
		}
		//Rückgabe des Strings des kürzesten Wegs
		return shortestWay;
      }
   }





